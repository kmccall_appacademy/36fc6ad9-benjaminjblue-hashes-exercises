# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
    hash = {}
    str.split.each do |word|
        hash[word] = word.length
    end
    hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
    largest_val = 0
    largest_key = nil
    hash.each do |key, value|
        if value > largest_val
            largest_val = value
            largest_key = key
        end
    end
    largest_key
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)

    newer.each do |key, value|
        older[key] = value
    end
    older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
    hash = Hash.new(0)
    word.each_char do |ch|
        hash[ch] += 1
    end
    hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
    hash = {}
    arr.select do |x|
        if hash[x]
            false
        else
            hash[x] = true
        end
    end
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
    hash = {odd: 0, even: 0}
    key = ['even'.to_sym, 'odd'.to_sym]
    numbers.each {|x| hash[key[x % 2]] += 1 }
    hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
    hash = Hash.new(0)
    is_vowel = 'aeiou'.each_char.reduce({}) do |is_vowel, ch|
        is_vowel[ch] = true
        is_vowel
    end
    string.each_char do |ch|
        hash[ch] += 1 if is_vowel[ch]
    end
    counts = hash.map {|key, value| [key, value]}.sort_by{|a|  -1 * a[1]}
    counts.select{|x| x[1] == counts[0][1]}.sort_by{|a| a[0]}[0][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
    #without using permutation
    second_half = students.select{|key, value| value > 6}.keys
    perms = []
    for i in 0...second_half.length
        for j in (i + 1)...second_half.length
            perms.push([second_half[i], second_half[j]])
        end
    end
    perms
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
    hash = Hash.new(0)
    specimens.each do |animal|
        hash[animal] += 1
    end
    res = hash.length ** 2
    max, min = 0, specimens.length
    hash.each do |key, value|
        min = value if value < min
        max = value if value > max
    end
    res * min / max
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
    count_hash = Hash.new(0)
    normal_sign.downcase.each_char do |ch|
        count_hash[ch] += 1
    end
    vandalized_sign.downcase.each_char do |ch|
        if count_hash[ch] < 1
            return false
        else
            count_hash[ch] -= 1
        end
    end
    true
end